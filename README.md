# Observing Scenario Paper 2019-2020 update

Files with .txt extension are two-column frequency, ASD ASCII files.

Files with .ecsv extension have ECSV comments lines in the file headers.

***

These curves were used to generate Figure 1 in the recent update to the
Observing Scenarios paper (see https://dcc.ligo.org/LIGO-P1200087-v58).  

Curves used for the simulations in the Observing Scenarios paper can be found in
a public filecard in the LIGO Document Control Center (DCC) at
https://dcc.ligo.org/LIGO-T2000012/public. 

The LIGO A+ curve, **AplusDesign.txt**, comes from https://dcc.ligo.org/LIGO-T1800042

The LIGO O1 and O2 curves are copied from the o1-sensitivity-curves and
o2-sensitivity-curves repositories. Further details on their origins are in the
README files for those repositories.

The **aligo_O3actual_L1.txt** curve is from https://dcc.ligo.org/LIGO-G1900992, the
**aligo_o3actual_H1.txt** curve is from https://dcc.ligo.org/LIGO-G1900993. These
curves were generated during the O3 run to act as reference spectra on the
summary pages and elsewhere. They are **not** identical to the spectra
found in https://git.ligo.org/sensitivity-curves/o3-sensitivity-curves.


Virgo and KAGRA curves are provided by their respective collaborations.
